﻿using Microsoft.Extensions.Logging;

namespace ShaderLS
{
    public interface IShaderLSEnvironment
    {
        LogLevel LogLevel { get; }
        int HostProcessId { get; }
        string TargetDirectory { get; }
        string SolutionFilePath { get; }
        string SharedDirectory { get; }
        string[] AdditionalArguments { get; }
    }
}
