@echo off


dotnet add Server.csproj package OmniSharp.Extensions.LanguageServer
dotnet add Server.csproj package GuiLabs.Language.Xml
dotnet add package Serilog.AspNetCore
