﻿using Microsoft.Extensions.Logging;

namespace ShaderLS
{
    public class BufferManager
    {
        private readonly ShaderLabWorkspace _workspace;
        private readonly ILogger<BufferManager> _logger;

        private Dictionary<string, string> documents = new Dictionary<string, string>();

        public BufferManager(ShaderLabWorkspace workspace, ILoggerFactory loggerFactory)
        {
            _workspace = workspace;
            _logger = loggerFactory.CreateLogger<BufferManager>();
        }

        public async Task UpdateBufferAsync()
        {
            
        }

        public void AddBuffer(string buffer, string filename)
        {
            _logger.LogInformation("Add buffer %s", filename);
            documents.Add(filename, buffer);
        }

        public void RemoveBuffer(string filename)
        {
            documents.Remove(filename);
        }
    }
}
